# Проект Forkio

---

___

***

## ___Список використаних технологій___:

1. HTML;
2. CSS;
3. SCSS;
4. JavaScript;
5. Gulp;
6. GIT

## ___Склад учасників проекту___:

1. Микола Борисенко (fe_14_online);
2. Олександр Поплавський (fe_14_online).

## ___Виконання завдань___:

![header](/header.png)
![editor](/editor.png)
![you-get](/you-get.png)
![about-fork](/about-fork.png)
![pricing](/pricing.png)

[link to photo with google drive](https://drive.google.com/file/d/16XxJ3iFM-wJu54jVbouYJcvX1ahSS4a5/view?usp=share_link)

![Earth](https://www.sciencealert.com/images/2020-04/processed/earthmovement_1024.jpg)

hyperlink --- [Google](https://www.google.com)

`hyperlink` --- [Google](https://www.google.com "підсказка Google")
